'''
=========================
Made by amir wolberg
Classes used: socket and thread
=========================
'''


from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import os


'''
================
Functions Below!
||||||||||||||||
VVVVVVVVVVVVVVVV
================
'''

def incoming_connections():
    """takes care of incoming clients."""
    while True:
        client, client_address = SERVER.accept()
        print("%s:%s has connected." % client_address)
        client.send("Welcome to Quick Chat! , Type your name in the Box below and press Enter to join the chat.")
        addresses[client] = client_address
        Thread(target=Client, args=(client,)).start()


def Client(client):  # Takes client socket as argument.
    """Takes care of one single client connection ( at a time) !"""
    global Logged_Clients
    name = client.recv(BUFSIZ).decode("utf8")
    Logged_Clients.append(str(name))
    Greeting = ('Welcome %s! , If you want to disconnect at any point type /exit.' % name )
    client.send(Greeting)
    #>>>>prints list of online clients as of the moment the client logs on<<<<<
    temp_client_string = '' #temporary string made to hold the names of the logged clients to print them
    for x in range(0 , len(Logged_Clients)):
        temp_client_string = temp_client_string + str(Logged_Clients[x]) + " , " #copying the logged_clients list to the temporary string
    client.send('Currently the users online are: ' + temp_client_string)
    # ====================================================================================================================
    msg = "%s has joined the chat!" % name
    #Writes msg in History.txt
    open_file= open('History.txt' ,'a')  #Writing the chat history into a file - user logged
    open_file.write(str(msg) + "\n")

    broadcast(bytes(msg))
    clients[client] = name

    while True:
        msg = client.recv(BUFSIZ)
        if msg != "/exit" :
            open_file.write(str(name) + ": " + str(msg) + "\n") #Writing the chat history into a file - msgs sent
            broadcast(msg, name + ": ")
        else:
            client.send("/exit")
            Logged_Clients.remove(str(name)) #removing the name of the client who disconnected from the logged_clients list
            client.close()
            del clients[client]
            open_file.write("%s has disconnected from the chat." % name + "\n") #Writing the chat history into a file - user disconnecting
            broadcast("%s has disconnected from the chat." % name)
            break




def broadcast(msg, prefix=""):  # prefix is for name identification.
    """Broadcasts a message to all the clients."""
    for sock in clients:
        sock.send(bytes(prefix) + msg)


'''
===============================
Global paramaters & Main Below!
|||||||||||||||||||||||||||||||
VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
===============================
'''


# >>>>>Global Parameters<<<<<
clients = {}  # list of clients
Logged_Clients = [] #list of all currently logged clients
addresses = {}  # list of clients's addresses


HOST = ''
PORT = 5001
BUFSIZ = 4096  # The buffer size
ADDR = (HOST, PORT)
# ===========================================================================


# >>>>>Socket<<<<<
SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDR)
# ===========================================================================


if __name__ == "__main__":
    SERVER.listen(256)
    print ("Working and waiting for a connection!")
    ACCEPT_THREAD = Thread(target=incoming_connections)  # accepts any incoming connections <<--
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join()
    SERVER.close()
