'''
Made by amir wolberg
Classes used: socket, thread, tkinter and pygame
'''

from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
from Tkinter import *
import Tkinter
from pygame import mixer  # for the sound effect


'''
================
Functions Below!
||||||||||||||||
VVVVVVVVVVVVVVVV
================
'''


def msg_send(event=None):  # event is passed by binders.
    """Function dedicated to the sending of msgs"""
    msg = my_msg.get()
    my_msg.set("")  # Clears input field.
    if msg not in chr(10):
        client_socket.send(msg)
        release_fart()
    if msg == "/exit":
        client_socket.close()
        GUI.quit()


def receive():
    """function dedicated to receiving msgs"""
    while True:
        try:
            msg = client_socket.recv(BUFSIZ).decode("utf8")
            msg_list.insert(Tkinter.END, msg)
        except OSError:  # Possibly client has left the chat.
            break


def Window_closed(event=None):
    """ When the window closes this function is called (incase the user presses the X in the top right instead of typing /exit)"""
    my_msg.set("/exit")
    msg_send()



def release_fart():  # Whenever you send a msg it sounds like you truly could'nt hold it back anymore
    mixer.init()
    mixer.music.load('msg_fart.mp3')
    mixer.music.play()

'''
=========================
Tkinter/Gui & Main Below!
|||||||||||||||||||||||||
VVVVVVVVVVVVVVVVVVVVVVVVV
=========================
'''


# >>>>> Tkinter&GUI section <<<<<
GUI= Tkinter.Tk()
GUI.configure(background='black') #here you may decide the color of the lower part
GUI.title("Quick Chat")
#image = PhotoImage(file = "logo.png")
#logo = Label(GUI , image= image) #doesn't work might try to fix at a later date
#logo.pack(side = Tkinter.RIGHT , fill = Tkinter.x)

msg_frame = Tkinter.Frame(GUI)
my_msg = Tkinter.StringVar()  # For the messages to be sent.
scrollbar = Tkinter.Scrollbar(msg_frame)  # Scrolling to see past msgs
# Following will contain the messages.
msg_list = Tkinter.Listbox(msg_frame, height=15, width=80, yscrollcommand=scrollbar.set , bg = 'black' , fg ='snow') #here you can decide the color of the upper background and the color of the text (bg for back ground and fg for text)
scrollbar.pack(side=Tkinter.RIGHT, fill=Tkinter.Y)                                                                   #you can also decide the size of the window
msg_list.pack(side=Tkinter.LEFT, fill=Tkinter.BOTH)
msg_list.pack()
msg_frame.pack()
entry_field = Tkinter.Entry(GUI, textvariable=my_msg)
entry_field.bind("<Return>", msg_send)
entry_field.pack()
send_button = Tkinter.Button(GUI, text=">>>", command=msg_send , bg = 'alice blue' , fg = 'red') #here you can decide the color of the button(bg) and the color of the text in it (fg)

send_button.pack()
GUI.protocol("WM_DELETE_WINDOW", Window_closed)
# =================================================================

# >>>>> Handling socket <<<<<
HOST = '127.0.0.1'  # IP of the server
PORT = 5001
BUFSIZ = 2048
ADDR = (HOST, PORT)

client_socket = socket(AF_INET, SOCK_STREAM)
client_socket.connect(ADDR)
# ===============================================

receive_thread = Thread(target=receive)
receive_thread.start()
Tkinter.mainloop()  # Starts GUI execution.
